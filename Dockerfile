# Use the official OpenJDK base image
FROM amazoncorretto:17

# Set the working directory inside the container
WORKDIR /app

# Copy the packaged JAR file into the container
COPY target/restaurant-application.jar app.jar

# Install MySQL client (for debugging and testing purposes)
RUN yum install -y mysql \
    && yum clean all

# Add wait-for-it script
ADD https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh wait-for-it.sh
RUN chmod +x wait-for-it.sh

# Expose the port the app will run on
EXPOSE 8080

# Command to run your application
CMD ["./wait-for-it.sh", "mysql-db:3306", "--", "java", "-jar", "app.jar"]

