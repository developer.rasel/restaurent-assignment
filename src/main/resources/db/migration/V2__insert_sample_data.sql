-- Insert Dummy Data into Customers Table
INSERT INTO customers (first_name, last_name, email)
VALUES
    ('John', 'Doe', 'john.doe@example.com'),
    ('Jane', 'Smith', 'jane.smith@example.com'),
    ('Bob', 'Johnson', 'bob.johnson@example.com'),
    ('Alice', 'Williams', 'alice.williams@example.com'),
    ('Charlie', 'Brown', 'charlie.brown@example.com');

-- Insert Dummy Data into Orders Table
INSERT INTO orders (customer_id, order_date, order_amount)
VALUES
    (1, '2024-02-03', 100.50),
    (2, '2024-02-03', 75.20),
    (3, '2024-02-03', 150.75),
    (4, '2024-02-03', 200.00),
    (5, '2024-02-03', 50.30);

-- Insert Dummy Data into Sales Table
INSERT INTO sales (order_id, sale_amount, sale_date)
VALUES
    (1, 90.50, '2024-02-03'),
    (2, 70.20, '2024-02-03'),
    (3, 140.75, '2024-02-03'),
    (4, 180.00, '2024-02-03'),
    (5, 45.30, '2024-02-03');
