-- Create Customers Table
CREATE TABLE customers (
                           customer_id BIGINT PRIMARY KEY AUTO_INCREMENT,
                           first_name VARCHAR(255) NOT NULL,
                           last_name VARCHAR(255) NOT NULL,
                           email VARCHAR(255) UNIQUE NOT NULL
);

-- Create Orders Table
CREATE TABLE orders (
                        order_id BIGINT PRIMARY KEY AUTO_INCREMENT,
                        customer_id BIGINT,
                        order_date DATE NOT NULL,
                        order_amount DECIMAL(10, 2) NOT NULL,
                        FOREIGN KEY (customer_id) REFERENCES customers(customer_id)
);

-- Create Sales Table
CREATE TABLE sales (
                       sale_id BIGINT PRIMARY KEY AUTO_INCREMENT,
                       order_id BIGINT,
                       sale_amount DECIMAL(10, 2) NOT NULL,
                       sale_date DATE NOT NULL,
                       FOREIGN KEY (order_id) REFERENCES orders(order_id)
);
