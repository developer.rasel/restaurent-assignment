package com.wallstreet.restaurantassignment.serviceImpl;

import com.wallstreet.restaurantassignment.entity.Customer;
import com.wallstreet.restaurantassignment.repository.CustomerRepository;
import com.wallstreet.restaurantassignment.services.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }


}
