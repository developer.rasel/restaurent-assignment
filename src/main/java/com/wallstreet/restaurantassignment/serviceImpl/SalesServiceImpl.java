package com.wallstreet.restaurantassignment.serviceImpl;

import com.wallstreet.restaurantassignment.entity.Sale;
import com.wallstreet.restaurantassignment.repository.SalesRepository;
import com.wallstreet.restaurantassignment.services.SaleService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
public class SalesServiceImpl implements SaleService {
    private final Logger logger = LogManager.getLogger(SalesServiceImpl.class);

    private final SalesRepository salesRepository;

    public SalesServiceImpl(SalesRepository salesRepository) {
        this.salesRepository = salesRepository;
    }

    @Override
    public String getMaxSaleDayInTimeRange(LocalDate startDate, LocalDate endDate) {
        try {
            List<Sale> salesList = salesRepository.findBySaleDateBetween(startDate, endDate);

            Optional<Sale> maxSale = salesList.stream()
                    .max(Comparator.comparing(Sale::getSaleAmount));

            if (maxSale.isPresent()) {
                LocalDate maxSaleDate = maxSale.get().getSaleDate();

                // Format LocalDate to a human-readable format
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formattedMaxSaleDate = maxSaleDate.format(formatter);

                logger.info("Max sale date in the time range ({} to {}): {}", startDate, endDate, formattedMaxSaleDate);

                return "Max Sale Date: " + formattedMaxSaleDate;
            } else {
                logger.info("No sales in the given time range ({} to {})", startDate, endDate);
                return "No sales in the given time range";
            }
        } catch (Exception e) {
            logger.error("Error while fetching max sale day in time range", e);
            throw new RuntimeException("Error while fetching max sale day in time range");
        }
    }

    @Override
    public BigDecimal getTotalSaleAmountForDay(LocalDate date) {
        try {
            LocalDate startOfDay = date.atStartOfDay().toLocalDate();
            LocalDate endOfDay = date.atTime(23, 59, 59, 999999999).toLocalDate();

            List<Sale> salesList = salesRepository.findBySaleDateBetween(startOfDay, endOfDay);

            BigDecimal totalSaleAmount = salesList.stream()
                    .map(Sale::getSaleAmount)
                    .reduce(BigDecimal.ZERO, BigDecimal::add);

            logger.info("Total sale amount for {} : {}", date, totalSaleAmount);

            return totalSaleAmount;
        } catch (Exception e) {
            logger.error("Error while fetching total sale amount for the day", e);
            throw new RuntimeException("Error while fetching total sale amount for the day");
        }
    }
}
