package com.wallstreet.restaurantassignment.serviceImpl;

import com.wallstreet.restaurantassignment.entity.Order;
import com.wallstreet.restaurantassignment.repository.OrderRepository;
import com.wallstreet.restaurantassignment.services.OrderService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    private final Logger logger = LogManager.getLogger(OrderServiceImpl.class);

    private final OrderRepository orderRepository;

    public OrderServiceImpl(OrderRepository orderRepository){
        this.orderRepository=orderRepository;
    }

    @Override
    public List<Order> getOrdersForCurrentDay() {
        try {
            LocalDate currentDate = LocalDate.now();
            LocalDate startOfDay = currentDate.atStartOfDay().toLocalDate();
            LocalDate endOfDay = currentDate.atTime(23, 59, 59, 999999999).toLocalDate();

            logger.info("Fetching orders for the current day ({} to {})", startOfDay, endOfDay);

            List<Order> orders = orderRepository.findByOrderDateBetween(startOfDay, endOfDay);

            logger.info("Successfully fetched {} orders for the current day", orders.size());
            return orders;
        } catch (Exception e) {
            logger.error("Error while fetching orders for the current day", e);
            throw new RuntimeException("Error while fetching orders for the current day");
        }
    }

    @Override
    public List<Order> getCustomerOrders(Long customerId) {
        try {
            logger.info("Fetching orders for customer with ID: {}", customerId);

            List<Order> customerOrders = orderRepository.findByCustomer_CustomerId(customerId);

            logger.info("Successfully fetched {} orders for customer with ID: {}", customerOrders.size(), customerId);
            return customerOrders;
        } catch (Exception e) {
            logger.error("Error while fetching customer orders", e);
            throw new RuntimeException("Error while fetching customer orders");
        }
    }
}
