package com.wallstreet.restaurantassignment.services;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public interface SaleService {
    String getMaxSaleDayInTimeRange(LocalDate startDate, LocalDate endDate);

    BigDecimal getTotalSaleAmountForDay(LocalDate date);
}
