package com.wallstreet.restaurantassignment.services;

import com.wallstreet.restaurantassignment.entity.Customer;
import com.wallstreet.restaurantassignment.entity.Order;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface CustomerService {
    List<Customer> getAllCustomers();

}
