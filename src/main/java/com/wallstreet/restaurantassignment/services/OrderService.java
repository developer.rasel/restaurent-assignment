package com.wallstreet.restaurantassignment.services;

import com.wallstreet.restaurantassignment.entity.Customer;
import com.wallstreet.restaurantassignment.entity.Order;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
public interface OrderService {
    List<Order> getOrdersForCurrentDay();

    List<Order> getCustomerOrders(Long customerId);
}
