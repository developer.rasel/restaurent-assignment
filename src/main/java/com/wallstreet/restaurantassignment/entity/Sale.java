package com.wallstreet.restaurantassignment.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "sales")
public class Sale implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sale_id")
    private Long saleId;

    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;

    @Column(name = "sale_amount", nullable = false)
    @NotNull(message = "Sale amount cannot be null")
    @DecimalMin(value = "0.01", message = "Sale amount must be greater than or equal to 0.01")
    @DecimalMax(value = "999999.99", message = "Sale amount must be less than or equal to 999999.99")
    private BigDecimal saleAmount;

    @Column(name = "sale_date", nullable = false)
    @NotNull(message = "Sale date cannot be null")
    private LocalDate saleDate;

}
