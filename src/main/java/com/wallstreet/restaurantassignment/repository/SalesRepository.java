package com.wallstreet.restaurantassignment.repository;

import com.wallstreet.restaurantassignment.entity.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface SalesRepository extends JpaRepository<Sale,Long> {
    List<Sale> findBySaleDateBetween(LocalDate startDateTime, LocalDate endDateTime);
}
