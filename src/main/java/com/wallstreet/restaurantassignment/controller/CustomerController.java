package com.wallstreet.restaurantassignment.controller;

import com.wallstreet.restaurantassignment.config.ApiResponse;
import com.wallstreet.restaurantassignment.entity.Customer;
import com.wallstreet.restaurantassignment.services.CustomerService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerController {
    private final Logger logger = LogManager.getLogger(CustomerController.class);
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService){
        this.customerService=customerService;
    }

    // Retrieve and return all customers
    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<ApiResponse<List<Customer>>> getAllCustomers() {
        try {
            logger.info("Fetching all customers");

            List<Customer> customers = customerService.getAllCustomers();

            logger.info("Successfully fetched {} customers", customers.size());
            return ResponseEntity.ok(ApiResponse.success("Customers retrieved successfully", customers));
        } catch (Exception e) {
            logger.error("Error while fetching customers", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ApiResponse.error("Error while fetching customers"));
        }
    }

}
