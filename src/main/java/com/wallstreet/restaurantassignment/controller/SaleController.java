package com.wallstreet.restaurantassignment.controller;

import com.wallstreet.restaurantassignment.config.ApiResponse;
import com.wallstreet.restaurantassignment.services.SaleService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDate;

@RestController
@RequestMapping("/api/sales")
public class SaleController {

    private final Logger logger = LogManager.getLogger(SaleController.class);

    private final SaleService saleService;

    public SaleController(SaleService saleService) {
        this.saleService = saleService;
    }

    // Retrieve and return the max sale day within the specified time range
    @GetMapping("/max-sale-day")
    public ResponseEntity<ApiResponse<String>> getMaxSaleDay(
            @RequestParam LocalDate startDate,
            @RequestParam LocalDate endDate) {

        // Validate input date range
        if (startDate == null || endDate == null || startDate.isAfter(endDate)) {
            logger.warn("Invalid date range received - start date: {}, end date: {}", startDate, endDate);
            return ResponseEntity.badRequest().body(ApiResponse.error("Invalid date range"));
        }

        // Validate date formats
        if (!isValidDate(startDate) || !isValidDate(endDate)) {
            logger.warn("Invalid date format received - start date: {}, end date: {}", startDate, endDate);
            return ResponseEntity.badRequest().body(ApiResponse.error("Invalid date format"));
        }

        try {
            logger.info("Received request for max sale day with start date {} and end date {}", startDate, endDate);

            String maxSaleDay = saleService.getMaxSaleDayInTimeRange(startDate, endDate);

            logger.info("Max sale day response: {}", maxSaleDay);

            return ResponseEntity.ok(ApiResponse.success("Max sale day retrieved successfully", maxSaleDay));
        } catch (Exception e) {
            logger.error("Error while fetching max sale day", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ApiResponse.error("Error while fetching max sale day"));
        }
    }

    // Retrieve and return the total sale amount for the specified day
    @GetMapping("/total-amount/current-day")
    public ResponseEntity<ApiResponse<BigDecimal>> getTotalSaleAmountForCurrentDay(
            @RequestParam(name = "date", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {

        // Validate date format
        if (date != null && !isValidDate(date)) {
            logger.warn("Invalid date format received - date: {}", date);
            return ResponseEntity.badRequest().body(ApiResponse.error("Invalid date format"));
        }

        try {
            if (date == null) {
                date = LocalDate.now();
            }

            BigDecimal totalSaleAmount = saleService.getTotalSaleAmountForDay(date);

            return ResponseEntity.ok(ApiResponse.success("Total sale amount for the current day retrieved successfully", totalSaleAmount));
        } catch (Exception e) {
            logger.error("Error while fetching total sale amount for the current day", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ApiResponse.error("Error while fetching total sale amount for the current day"));
        }
    }

    private boolean isValidDate(LocalDate date) {
        try {
            // Attempt to parse the date; if successful, the date is valid
            LocalDate.parse(date.toString());
            logger.debug("Date {} is valid", date);
            return true;
        } catch (Exception e) {
            logger.warn("Invalid date format for date {}", date);
            return false;
        }
    }
}
