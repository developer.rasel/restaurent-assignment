package com.wallstreet.restaurantassignment.controller;

import com.wallstreet.restaurantassignment.config.ApiResponse;
import com.wallstreet.restaurantassignment.entity.Order;
import com.wallstreet.restaurantassignment.services.OrderService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    private final Logger logger = LogManager.getLogger(OrderController.class);
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    // Retrieve and return orders for the current day
    @GetMapping("/current-day")
    public ResponseEntity<ApiResponse<List<Order>>> getCurrentDayOrders() {
        try {
            logger.info("Fetching orders for the current day");

            List<Order> orders = orderService.getOrdersForCurrentDay();

            logger.info("Successfully fetched {} orders for the current day", orders.size());
            return ResponseEntity.ok(ApiResponse.success("Orders retrieved successfully for the current day", orders));
        } catch (Exception e) {
            logger.error("Error while fetching current day orders", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ApiResponse.error("Error while fetching current day orders"));
        }
    }

    // Retrieve and return orders for a specific customer
    @GetMapping("/customer")
    public ResponseEntity<ApiResponse<List<Order>>> getCustomerOrders(
            @RequestParam Long customerId) {
        if (customerId == null || customerId <= 0) {
            logger.warn("Invalid customer ID received - Customer ID: {}", customerId);
            return ResponseEntity.badRequest()
                    .body(ApiResponse.error("Invalid customer ID received"));
        }
        try {
            logger.info("Fetching orders for customer with ID: {}", customerId);

            List<Order> customerOrders = orderService.getCustomerOrders(customerId);


            if (customerOrders.size()>0){
                logger.info("Successfully fetched {} orders for customer with ID: {}", customerOrders.size(), customerId);
                return ResponseEntity.ok(ApiResponse.success("Orders retrieved successfully for customer with ID: " + customerId, customerOrders));
            }else {
                logger.info("No Orders Available for customer with ID: {}", customerId);
                return ResponseEntity.ok(ApiResponse.success("No Orders Available for customer with ID: " + customerId, customerOrders));
            }

        } catch (Exception e) {
            logger.error("Error while fetching customer orders", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ApiResponse.error("Error while fetching customer orders"));
        }
    }
}
