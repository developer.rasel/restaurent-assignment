package com.wallstreet.restaurantassignment;

import com.wallstreet.restaurantassignment.entity.Customer;
import com.wallstreet.restaurantassignment.entity.Order;
import com.wallstreet.restaurantassignment.entity.Sale;
import com.wallstreet.restaurantassignment.repository.SalesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class SalesRepositoryTest {

    @Mock
    private SalesRepository salesRepository;

    @Test
    public void testFindBySaleDateBetween() {
        // Arrange
        LocalDate startDate = LocalDate.now().minusDays(7);
        LocalDate endDate = LocalDate.now();
        Sale sale1 = new Sale(1L,new Order(1L,new Customer(1L,"john","doe","john@gmail.com"),LocalDate.now(),BigDecimal.valueOf(50.0)),BigDecimal.valueOf(50.0),LocalDate.now()); // create Sale instances as needed
        Sale sale2 = new Sale(2L,new Order(2L,new Customer(2L,"kon","smith","smith@gmail.com"),LocalDate.now(),BigDecimal.valueOf(90.0)),BigDecimal.valueOf(90.0),LocalDate.now()); // create Sale instances as needed
        List<Sale> mockSales = Arrays.asList(sale1, sale2);

        Mockito.when(salesRepository.findBySaleDateBetween(Mockito.any(), Mockito.any()))
               .thenReturn(mockSales);

        // Act
        List<Sale> result = salesRepository.findBySaleDateBetween(startDate, endDate);

        // Assert
        assertEquals(mockSales.size(), result.size());

    }

}
