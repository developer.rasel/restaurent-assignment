package com.wallstreet.restaurantassignment;

import com.wallstreet.restaurantassignment.entity.Customer;
import com.wallstreet.restaurantassignment.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class CustomerRepositoryTest {

    @Mock
    private CustomerRepository customerRepository;

    @Test
    public void testFindAll() {
        // Arrange
        Customer customer1 = new Customer(); // create Customer instances as needed
        Customer customer2 = new Customer();
        List<Customer> mockCustomers = Arrays.asList(customer1, customer2);

        Mockito.when(customerRepository.findAll()).thenReturn(mockCustomers);

        // Act
        List<Customer> result = customerRepository.findAll();

        // Assert
        assertEquals(mockCustomers.size(), result.size());
        
    }

    
}
